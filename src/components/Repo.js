import React, { useState } from "react";
import Stack from "@mui/material/Stack";

import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Link from "@mui/material/Link";

const Repo = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClickMenuSearch = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleCloseMenuSearch = () => {
    setAnchorEl(null);
  };
  return (
    <div className="menu-menu">
      <Button
        id="menu-button"
        variant="contained"
        aria-controls={open ? "basic-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        onClick={handleClickMenuSearch}
        color={"secondary"}
      >
        Git Repo
      </Button>
      <Menu
        id="menu-menu-btn"
        anchorEl={anchorEl}
        open={open}
        onClose={handleCloseMenuSearch}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
        sx={{ marginTop: ".5rem" }}
      >
        <MenuItem>
          <Stack
            component="form"
            direction={{ xs: "column", sm: "column" }}
            spacing={{ xs: 1, sm: 2, md: 4 }}
          >
            <Link
              href="https://gitlab.com/sanudin/titik-keramaian-api"
              underline="always"
              target={"_blank"}
            >
              Gitlab repo API
            </Link>
            <Link
              href="https://gitlab.com/sanudin/titik-keramaian-frontend"
              underline="always"
              target={"_blank"}
            >
              Gitlab repo Frontend
            </Link>
          </Stack>
        </MenuItem>
      </Menu>
    </div>
  );
};

export default Repo;
