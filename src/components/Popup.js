import React from "react";

const Popup = ({ total_user, data_brand, range }) => {
  return (
    <div className="popup-information">
      <div className="header-popup">Information</div>
      <p>
        {range} <b>({total_user})</b>
      </p>
      <table>
        <tr>
          <th>Brand</th>
          <th>User</th>
        </tr>
        {data_brand.map((brand, i) => {
          return (
            <tr key={i}>
              <td>{brand.brand}</td>
              <td>{brand.user_per_brand}</td>
            </tr>
          );
        })}
      </table>
    </div>
  );
};

export default Popup;
