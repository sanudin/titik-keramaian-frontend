import { useState, useEffect } from "react";
import ReactDOMServer from "react-dom/server";
import axios from "axios";
import L from "leaflet";
import {
  MapContainer,
  TileLayer,
  ZoomControl,
  LayersControl,
} from "react-leaflet";
import Popup from "../components/Popup";
import Search from "../components/Search";
import Repo from "../components/Repo";

import "leaflet/dist/leaflet.css";
import "./App.css";

//reset marker icon
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
});
const { BaseLayer } = LayersControl;
var markers;

function App() {
  const [map, setMap] = useState(null); //save map
  const [center, setCenter] = useState([-6.1754, 106.8272]); //set center (monas)
  const [timeFrom, setTimeFrom] = useState(new Date("2021-10-20 07:00:00"));
  const [timeTo, setTimeTo] = useState(new Date("2021-10-20 08:00:00"));

  //handling API
  const getGeoJson = (bodyData, url = "coords") => {
    return axios
      .post(`https://titik-keramaian-api.herokuapp.com/${url}`, bodyData)
      .then((res) => {
        return res.data;
      })
      .catch((err) => {
        return err;
      });
  };

  //draw marker to the map
  const drawMarkerByGeoJson = (dataCoords) => {
    if (!dataCoords) return;

    if (markers) {
      map.removeLayer(markers);
    }

    markers = new L.GeoJSON(dataCoords, {
      onEachFeature: (feature = {}, layer) => {
        const { properties = {} } = feature;
        const { data_brand, total_user, range } = properties;

        if (!data_brand) return;

        const PopupEl = ReactDOMServer.renderToString(
          <Popup
            total_user={total_user}
            data_brand={data_brand}
            range={range}
          />
        );

        layer.bindPopup(`${PopupEl}`);
      },
      pointToLayer: (geoJsonPoint, latlng) => {
        const tUser = geoJsonPoint.properties.total_user;
        const markerColor = tUser > 8 ? "red" : tUser > 5 ? "orange" : "yellow";

        const myIconMarker = L.divIcon({
          className: `div-marker ${markerColor}`,
          html: `<span class="my-div-span">${tUser}</span>`,
          popupAnchor: [12, -15],
        });
        return L.marker(latlng, { icon: myIconMarker });
      },
    });

    markers.addTo(map);
  };

  //change view to new center coordinates
  const handleSetView = (coord) => {
    map.setView(coord, 15);
  };

  //get data when first load map
  useEffect(() => {
    if (!map) return;

    handleSearch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [map]);

  //Inputs
  const handleTimeFrom = (time) => {
    setTimeFrom(time);
  };
  const handleTimeTo = (time) => {
    setTimeTo(time);
  };
  const handleTime = (time) => {
    const _y = time.getFullYear();
    const _m =
      time.getMonth() + 1 < 10
        ? `0${time.getMonth() + 1}`
        : time.getMonth() + 1;
    const _d = time.getDate() < 10 ? `0${time.getDate()}` : time.getDate();
    const _h = time.getHours() < 10 ? `0${time.getHours()}` : time.getHours();

    return `${_y}-${_m}-${_d} ${_h}:00:00`;
  };

  const handleSearch = () => {
    const bodyData = {
      from: handleTime(timeFrom),
      to: handleTime(timeTo),
    };

    getGeoJson(bodyData)
      .then((dataCoords) => {
        drawMarkerByGeoJson(dataCoords);
        const newCenter = dataCoords.features[0].geometry.coordinates;
        setCenter(newCenter);
        handleSetView(newCenter.reverse());
      })
      .catch((err) => {
        return;
      });
  };

  return (
    <div className="App">
      <MapContainer
        whenCreated={(map) => {
          setMap(map);
        }}
        center={center}
        zoom={15}
        zoomControl={false}
        style={{
          height: "100vh",
          width: "100vw",
        }}
      >
        <LayersControl position="bottomright">
          <BaseLayer checked name="Light">
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
          </BaseLayer>
          <BaseLayer name="Dark">
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png"
            />
          </BaseLayer>
        </LayersControl>
        <ZoomControl position="bottomright" />
      </MapContainer>
      <Search
        timeFrom={timeFrom}
        timeTo={timeTo}
        handleTimeFrom={handleTimeFrom}
        handleTimeTo={handleTimeTo}
        handleSearch={handleSearch}
      />
      <Repo />
    </div>
  );
}

export default App;
